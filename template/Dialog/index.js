define(function(require, exports, module) {
    // 定义弹出模态框对象
    var SimpleDialog = {},
        mainTemplate = require('./dialogIndex'),
        addModal = require('./modal');
    // 模块初始化
    SimpleDialog.initModule = function() {
        console.log('初始化了');
        var html = mainTemplate();
        Tools.initModulePage(html);
        SimpleDialog.pageContent = $('.dialog-container');
        SimpleDialog.initEvent();
    };
    // 初始化事件
    SimpleDialog.initEvent = function() {
        // 初始化点击事件
        SimpleDialog.pageContent.off('click').on('click','.btn', function(event) {
            event.preventDefault();
            var that = $(this), target = that.data('target');
            if(target === 'open-modal') {
                var addHtml = addModal();
                $.modal({
                    title: '这是一个模态框',
                    content: addHtml,
                    confirm: function (modal) {
                        $.confirm({
                            title: '温馨提示',
                            content: '确定要删除吗',
                            confirm: function (modal) {
                                console.log('modal1', modal);
                            },
                            cancel: function() {
                                console.log('close1',this);
                            },
                            afterOpen: function() {
                                console.log('打开之后的回调1')
                            }
                        });
                    },
                    cancel: function() {
                        console.log('close',this);
                    }
                });	
            }
            if(target === 'open-message') {
                $.message({
                    content: '这是一个提示消息',
                });	
            }
        });
    }
    exports.init = SimpleDialog.initModule;
});