define(function(require, exports, module) {
  var mainHtml = require('./index');
  var Bookmark = {};
  Bookmark.initModule = function () {
    var nowDate = new Date(),
        nowDateStr = Tools.formatDate(nowDate),
        nowWeek = Tools.getWeek('2021-5-8');
    var data = {};
    data.scheduleLis = nowWeek;
    data.currentDate = nowDateStr;
    var html = mainHtml(data);
    Tools.initModulePage(html);

  };
  exports.init = Bookmark.initModule;
});