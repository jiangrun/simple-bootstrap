define(function(require, exports, module) {
  var mainHtml = require('./view/index');
  var Forum = {};
  Forum.initModule = function () {
    var data = {};
    var html = mainHtml(data);
    Tools.initModulePage(html);
    Forum.initEvent();
  };
  // 初始化事件
  Forum.initEvent = function() {
  };
  exports.init = Forum.initModule;
});