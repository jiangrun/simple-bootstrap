define(function(require, exports, module) {
  var mainHtml = require('./view/index'),
      addHtml = require('./view/add');
  var Top = {};
  Top.initModule = function () {
    var nowDate = new Date(),
        nowDateStr = Tools.formatDate(nowDate),
        nowWeek = Tools.getWeek(nowDateStr);
    var data = {};
    var hasData = ['Mon', 'Tue', 'Wed', 'Thu', 'Fry'];
    var scheduleData = [{
      week: 'Mon',
      schedule: [{
        id: 1,
        type: 'special',
        content: '11:00-12:00 グループ'
      },{
        id: 2,
        type: 'common',
        content: '会社'
      }]
    },{
      week: 'Tue',
      schedule: [{
        id: 1,
        type: 'common',
        content: '会社'
      }]
    },{
      week: 'Wed',
      schedule: [{
        id: 1,
        type: 'special',
        content: '11:00-12:00 グループ'
      },{
        id: 2,
        type: 'common',
        content: '会社'
      }]
    },{
      week: 'Thu',
      schedule: [{
        id: 1,
        type: 'common',
        content: '会社'
      }]
    },{
      week: 'Fry',
      schedule: [{
        id: 1,
        type: 'special',
        content: '11:00-12:00 グループ'
      },{
        id: 2,
        type: 'common',
        content: '会社'
      }]
    },]
    data.scheduleLis = nowWeek.map(function(item, index) {
      var schedule = scheduleData.find(function(child){return child.week === item.label}) 
      return {
        ...item,
        content: schedule ? schedule.schedule : []
      };
    });
    data.currentDate = Tools.formatDate(nowDate, '年');
    var html = mainHtml(data);
    Tools.initModulePage(html);
    Top.initEvent();
  };
  Top.initEvent = function() {
    $('.btn-outline-secondary').on('click', function(event) {
      event.preventDefault();
      var layerDemo = layer.open({
        type: 1,
        title: '这是一个标题',
        shadeClose: true, //开启遮罩关闭
        btn: ['确定','取消'],
        content: addHtml(),
        success: function() {
          // layer.msg('弹出后的回调')
        },
        area: ['500px', '300px'],
        yes: function(index, layerObj) {
          layer.msg('你点击了确定')
        },
      })
    });
    $('.msg').on('click', function(event) {
      event.preventDefault();
      var layerDemo = layer.msg('hello',{
        offset: '200px',
      });
    });
    $('.T-alert').on('click', function(event) {
      event.preventDefault();
      var layerDemo = layer.alert('hello');
    });
  }
  exports.init = Top.initModule;
});