## 1.nodeJs 脚本设置
### 1.1 初始化nodejs
	
	打开node.js command prompt  粘贴 npm install -g tmodjs 回车初始化

### 1.2 脚本文件路径设置

	打开node.js command prompt  粘贴 tmod E:\work\bootstrap_ui\template  --output E:\work\bootstrap_ui\template  --type cmd --debug 

### 1.3 tmod运行路径说明：
	1、yourWorkSpace：存放代码文件夹
	2、yourProjectName：项目工程命名
	3、yourTemplate：存放模块html文件夹

## 2 模板引擎 artTemplate
	使用示例请参考：https://blog.csdn.net/gao_xu_520/article/details/78595318

## 3 弹层组件 layer.js
	详细使用方法请参考：https://www.layui.com/doc/modules/layer.html