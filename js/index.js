(function(window, $) {
  $.message = function (options) {
    options = $.extend(options, {
      showHeader: false,
      showFooter: false
    });
    var option = $.extend({}, smipleConfirm.defaults, options);
    return smipleConfirm(option);
  };
  // 弹层模态框
  $.modal = function (options) {
    return smipleModal(options);
  }
  // 确认框
  // $.toast = function (options) {
  //   var mainContent = $('.toast');
  //   var 
  //   // 提示框头部
  //   var header = mainContent.find('.toast-header');
  //   // 提示框body
  //   var body = mainContent.find('.toast-body').html(that.content);
  //   // 显示自定义标题
  //   $(header).append('<strong class="mr-auto toast-title">'+ that.title +'</strong>')
  //   $(mainContent).find('.modal-content').append(body);
  // };
  
  var smipleModal = function(options) {
    var options = $.extend({}, smipleModal.defaults, options);
    return new ModalBoostrap(options);
  };
  var smipleConfirm = function(options) {
    options = $.extend(options, {
      showHeader: false,
      showFooter: true,
      showCloseBtn: false,
      type: 'confirm'
    });
    var options = $.extend({}, smipleModal.defaults, options);
    return new ConfirmByBoostrap(options);
  };
  var modalArr = [];
  var ModalBoostrap = function (options) {
    $.extend(this, options);
    this.init();
  };
  ModalBoostrap.prototype = {
    init: function() {
      var that = this;
      that.buildHtml();
      that.bindEvents();
    },
    buildHtml: function() {
      var that = this;
      var contentStr =  '<div class="modal shadow fade" id="simple-modal" tabindex="1000" data-backdrop="static">'+
                          '<div class="modal-dialog">'+
                            '<div class="modal-content">'+
                            '</div>'+
                          '</div>'+
                        '</div>';
      if(that.type === 'confirm') {
        contentStr =  '<div class="modal shadow fade" id="simple-confirm" data-type='+ that.type +'tabindex="1000" data-backdrop="false" style="display: block">'+
                          '<div class="modal-dialog">'+
                            '<div class="modal-content">'+
                            '</div>'+
                          '</div>'+
                        '</div>';
      };
      var mainContent = $(contentStr);

      // 模态框头部
      var header = $('<div class="modal-header"></div>');
      // 模态框body
      var body = $('<div class="modal-body">'+ that.content +'</div>');
      // 模态框footer
      var footer = $('<div class="modal-footer"></div>');
      // 显示自定义标题
      $(header).append('<div class="modal-title">'+ that.title +'</div>')
      // 控制显示关闭按钮
      if(that.showCloseBtn) {
        $(header).append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
      };
      // 是否显示头部
      if(that.showHeader) {
        $(mainContent).find('.modal-content').append(header);
      };
      $(mainContent).find('.modal-content').append(body);
      
      // 控制显示模态框底部
      if(that.showFooter) {
        that.btns.forEach(function (btn) {
          var btnclass = btn.class || "btn-primary";
          var _b = "";
          if (btn.id != "closebtn") {
              _b = $('<button class="btn btn-sm" type="button" data-type="'+ that.type +'" data-target="confirm" id="' + btn.id + '">' + btn.text + '</button>');
              $(_b).addClass(btnclass);
          } else {
              _b = $('<button class="btn btn-sm btn-secondary" data-dismiss="modal" data-target="close" type="button" id="' + btn.id + '">' + btn.text + '</button>');
          }
          $(footer).append(_b);
        }, this);
        $(mainContent).find('.modal-content').append(footer);
      };
      // 防止重复append
      if(!$(that.container).find('#simple-' + that.type).length) {
        $(that.container).append(mainContent);
      };
    },
    bindEvents: function() {
      var self = this;
      var currModal = $('#simple-modal');
      if(self.type === 'confirm') {
        currModal = $('#simple-confirm');
      }
      currModal.modal('show');
      // 打开弹层之后的回调
      currModal.off('shown.bs.modal').on('shown.bs.modal', function (event) {
        if(self.afterOpen) {
          self.afterOpen();
        };
      });
      // 关闭弹层销毁
      currModal.off('hidden.bs.modal').on('hidden.bs.modal', function (event) {
        self.cancel();
        currModal.css({
          display: 'none'
        });
        if(self.destroyOnClose) {
          currModal.remove();
        };
      });
      $('.modal-footer').off('click').on('click', '.btn', function(event) {
        event.preventDefault();
        var target = $(this).data('target'), that = this, type = $(this).data('type');
        // 绑定确认事件
        if(target === 'confirm') {
          if(self.confirm) {
            self.confirm(currModal);
          };
        };
      })
    }
  };
  // 弹层默认配置
  smipleModal.defaults = {
    // 弹层类型
    type: 'modal',
    // 弹层标题
    title: '模态框标题',
    // 点击确认回调
    confirm: function() {},
    // 点击取消回调
    cancel: function() {},
    // 弹出层append位置
    container: 'body',
    // 弹层主体
    content: '这是一个模态框',
    // 是否展示头部-message
    showHeader: true,
    // 是否展示关闭按钮
    showCloseBtn: true,
    // 是否展示底部
    showFooter: true,
    // 按钮组
    btns: [{id: 'confirm', text: '确定'}, {id: 'closebtn', text: '关闭'}],
    // 打开弹层之后的回调
    afterOpen: function() {},
    // 关闭弹层之后是否销毁
    destroyOnClose: false
  };
  // 初始化
	jQuery(document).ready(function($) {
    // 初始进入默认模块
    var target = sessionStorage.getItem('currentModule') || 'top';
    // 根据target添加active
    $('.navbar-nav').find('.nav-item').removeClass('active');
    $('.navbar-nav').find('.nav-item[data-target='+ target +']').addClass('active')
		seajs.use("./" + modalScripts[target],function(module){
      module.init();
    });
	});
})(window, jQuery)