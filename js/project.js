// 定义sea.js加载的组件模块
const modalScripts = {
  'top': 'template/Top/TopIndex.js',
  'schedule': 'template/Schedule/ScheduleIndex.js',
  'forum': 'template/Forum/ForumIndex.js'
};
// 定义工具类
var Tools = {};
// 绑定侧边点击事件
$('.navbar-nav').off('click').on('click', '.nav-item', function(event) {
    event.preventDefault();
    var that = this, target = $(that).data('target');
    $('.navbar-nav').find('.nav-item').removeClass('active');
    $(that).addClass("active");
    if(target && modalScripts[target]) {
      seajs.use("./" + modalScripts[target],function(module){
        sessionStorage.setItem('currentModule', target);
        module.init();
      });
    }
});
Tools.initModulePage = function (html) {
  var pageContent = $('.page-content');
  pageContent.html(html);
};
Tools.formatDate = function (date, formatFlag) {
  formatFlag = formatFlag || '-'
  return date.getFullYear() + formatFlag + (date.getMonth() + 1) + formatFlag + date.getDate();
}
Tools.getWeek = function (dateString) {
    let dateStringReg = /^\d{4}[/-]\d{1,2}[/-]\d{1,2}$/;
    if (dateString.match(dateStringReg)) {
        let presentDate = new Date(dateString),
            today = presentDate.getDay() !== 0 ? presentDate.getDay() : 7;
        let week = {
          6: 'Sun',
          0: 'Mon',
          1: 'Tue',
          2: 'Wed',
          3: 'Thu',
          4: 'Fry',
          5: 'Sat'
        }
        return Array.from(new Array(7), function(val, index) {
          // let formatStr = formatDate(new Date(presentDate.getTime() - (today - index-1) * 24 * 60 * 60 * 1000))
          let formatStr = new Date(presentDate.getTime() - (today - index-1) * 24 * 60 * 60 * 1000);
          let obj = {
            label: week[index],
            value: formatDate(formatStr, '-'),
            days: formatStr.getDate(),
            isToday: today === index + 1
          };
          return obj;
        });

    } else {
        throw new Error('dateString should be like "yyyy-mm-dd" or "yyyy/mm/dd"');
    }
    function formatDate(date, formatFlag) {
        return date.getFullYear() + formatFlag + (date.getMonth() + 1) + formatFlag + date.getDate();
    }
}